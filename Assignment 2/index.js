Array.prototype.swap = function (x, y) {
  let b = this[x];
  this[x] = this[y];
  this[y] = b;
  return this;
};

function partitionAsc(arr, param, l, h) {
  const pivot = arr[h][param];

  let i = l - 1;
  for (let j = l; j < h; j++) {
    if (arr[j][param] < pivot) {
      i++;
      arr.swap(i, j);
    }
  }

  arr.swap(i + 1, h);

  return i + 1;
}

function partitionDesc(arr, param, l, h) {
  const pivot = arr[h][param];
  let i = l - 1;
  for (let j = l; j < h; j++) {
    if (arr[j][param] > pivot) {
      i++;
      arr.swap(i, j);
    }
  }

  arr.swap(i + 1, h);

  return i + 1;
}

function quickSort(array, param, order, l, h) {
  if (l < h) {
    let p =
      order === 'asc'
        ? partitionAsc(array, param, l, h)
        : partitionDesc(array, param, l, h);
    quickSort(array, param, order, l, p - 1);
    quickSort(array, param, order, p + 1, h);
  }
}

class Person {
  constructor(name, age, salary, sex) {
    this.name = name;
    this.age = age;
    this.salary = salary;
    this.sex = sex;
  }

  static sort(arr, param, order) {
    quickSort(arr, param, order, 0, arr.length - 1);
    return arr;
  }
}

const person1 = new Person('jeff', 22, 25000, 'male');
const person2 = new Person('bill', 28, 35800, 'male');
const person3 = new Person('jennifer', 21, 14800, 'female');
const person4 = new Person('vicky', 34, 98500, 'male');
const person5 = new Person('may', 29, 47500, 'female');

const persons = [person1, person2, person3, person4, person5];

console.log(Person.sort(persons, 'age', 'asc'));
// console.log(Person.sort(persons, 'age', 'desc'));
