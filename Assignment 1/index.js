document.getElementById('frm').addEventListener('submit', onSubmit);

function onSubmit(event) {
  event.preventDefault();
  validateEmail(event);
  validatePassword(event);
  validateSex(event);
  validateRole(event);
  validatePerm(event);
  const boxes = document.getElementsByTagName('input');
  let permission = [];
  for (let i = 0; i < boxes.length; i++) {
    if (boxes[i].type.toLowerCase() === 'checkbox' && boxes[i].checked) {
      const per = boxes[i].labels[0].innerHTML;
      permission.push(per);
    }
  }
  let data = {
    email: event.target.elements.email.value,
    password: event.target.elements.pass.value,
    sex: event.target.elements.sex.value,
    role: document.getElementById('admin').checked ? 'admin' : 'user',
    permissions: [...permission]
  };

  document.getElementById('frm').classList.add('hidden');
  createNewScreen(data);
}

function createNewScreen(data) {
  const parent = document.createElement('div');
  parent.classList.add('frm');
  const email = document.createElement('div');
  email.innerHTML = data.email;
  const password = document.createElement('div');
  password.innerHTML = data.password;
  const sex = document.createElement('div');
  sex.innerHTML = data.sex;
  const role = document.createElement('div');
  role.innerHTML = data.role;
  const permission = document.createElement('div');
  const permi = data.permissions + '';
  const perm = document.createTextNode(permi);
  permission.appendChild(perm);

  parent.appendChild(email);
  parent.appendChild(password);
  parent.appendChild(sex);
  parent.appendChild(role);
  parent.appendChild(permission);
  const confirm = document.createElement('button');
  confirm.innerHTML = 'Confirm';
  parent.appendChild(confirm);
  document.getElementsByTagName('main')[0].appendChild(parent);
}

function validateEmail(event) {
  if (
    !event.target.elements.email.value
      .toLowerCase()
      .match(
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      )
  ) {
    document.getElementById('email-valid-error').classList.remove('hidden');
  } else {
    document.getElementById('email-valid-error').classList.add('hidden');
  }
}

function validatePassword(event) {
  if (
    !event.target.elements.pass.value.match(
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{6,}$/
    )
  ) {
    document.getElementById('pw-valid-error').classList.remove('hidden');
  } else {
    document.getElementById('pw-valid-error').classList.add('hidden');
  }
}

function validateSex(event) {
  if (event.target.elements.sex.value === '') {
    document.getElementById('sex-error').classList.remove('hidden');
  } else {
    document.getElementById('sex-error').classList.add('hidden');
  }
}

function validateRole(event) {
  if (
    !document.getElementById('admin').checked &&
    !document.getElementById('user').checked
  ) {
    document.getElementById('role-error').classList.remove('hidden');
  } else {
    document.getElementById('role-error').classList.add('hidden');
  }
}

function validatePerm(event) {
  let checked = checkboxes();
  if (checked < 2) {
    document.getElementById('perm-error').classList.remove('hidden');
  } else {
    document.getElementById('perm-error').classList.add('hidden');
  }
}

function checkboxes() {
  let checked = 0;
  const boxes = document.getElementsByTagName('input');
  for (let i = 0; i < boxes.length; i++) {
    if (boxes[i].type.toLowerCase() === 'checkbox' && boxes[i].checked) {
      checked++;
    }
  }
  return checked;
}
